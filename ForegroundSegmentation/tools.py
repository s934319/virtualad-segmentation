import cv2,os
import numpy as np
import shutil
def Video2Frames(video_dir,output_dir,resolution = 720):
    def NumberString(counter:int):
        if(counter < 10):
            return '0000' + str(counter)
        if(counter < 100):
            return '000'  + str(counter)
        if(counter < 1000):
            return '00'   + str(counter)
        if(counter < 10000):
            return '0'    + str(counter)
    image_list = []
    vidcap = cv2.VideoCapture(video_dir)
    success,image = vidcap.read()
    count = 0
    success = True
    while success:
        success,image = vidcap.read()
        counter = NumberString(count)
        if(success):
            image = cv2.resize(image,(int(resolution/9*16),resolution))
            cv2.imwrite(os.path.join(output_dir,counter) + '.jpg', image)     # save frame as JPEG file
            image_list.append(image)
        count += 1
    return image_list

def pitcher_index(anno_mask):
    def y_lowerbound(mask):
        doko = np.where(mask)
        return doko[0].max()
    max_y = 0
    max_idx = -1
    mask_count = int(anno_mask.max()/10)
    for idx in range(mask_count):
        mask = anno_mask == (idx+1)*10
        if(y_lowerbound(mask) > max_y):
            max_y = y_lowerbound(mask)
            max_idx = idx
    return max_idx

def graymask(people_masks):
    mask_img = np.zeros(people_masks.shape[1:])
    for idx,mask in enumerate(people_masks):
        doko = np.where(mask)
        mask_img[doko] = (idx+1)*10
    mask_img = mask_img.astype(np.uint8)
    return mask_img

def Frames2Video(frame_list:list,output_dir,video_name,frame_rate:int):
    fulldir = os.path.join(output_dir,video_name) + '.mp4'
    resolution = frame_list[0].shape[1],frame_list[0].shape[0]
    out = cv2.VideoWriter(fulldir,cv2.VideoWriter_fourcc(*'MP4V'), frame_rate, resolution)
    for img in frame_list:
        if(len(img.shape) == 2):
            img2 = cv2.cvtColor(img,cv2.COLOR_GRAY2RGB)
        else:
            img2 = img
        out.write(img2)
    out.release()

def Gray2Depth(mask,pitcher_index):
    pitcher_index = (pitcher_index + 1)*10
    depth_mask = np.zeros(mask.shape)
    indexs = np.unique(mask)[1:]
    for index in indexs:
        if(index == pitcher_index):
            depth_mask[np.where(mask == index)] = 0.7
        else:
            depth_mask[np.where(mask == index)] = 0.3
    depth_mask = (depth_mask*255).astype(np.uint8)
    return depth_mask

def bounding_box(mask):
    doko = np.where(mask)
    return [int(doko[1].min()),int(doko[1].max()),int(doko[0].min()),int(doko[0].max())]

def DeleteFolder(folder_dir):
    shutil.rmtree(folder_dir)