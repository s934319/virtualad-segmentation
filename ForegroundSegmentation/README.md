# Instance Segmentation README
## Environment Setting
* OS: ubuntu (16, 18)
* Anaconda: https://www.anaconda.com/products/individual#Downloads
* you need to install Detectron2 and its depending packages (e.g. pytorch)


#### 參數說明
1. **input**: 存放原始影片的資料夾位置

#### 其他事項
1. 計算好的影片會儲存在"result"資料夾
2. 計算結果包含原始影片、遮罩影片、切割資訊、平面法向量
3. 需要將計算後的影片資料夾放入UnityEditor的"resources"資料夾以進行編輯
4. 能計算的影片長度會視顯存容量而定

### Use pipeline
```
python pipeline.py --input <video_dir>
```

