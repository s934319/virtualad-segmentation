import numpy as np
import matplotlib.pyplot as plt
import open3d as o3d
import cv2

def read_file(fname):
        if fname.endswith('.npy'):
            return np.load(fname)
        elif fname.endswith('.jpg') or fname.endswith('.jpeg'):
            img = plt.imread(fname)
            img = img.astype(np.float32) / 255.0
            return img
        elif fname.endswith('.png'):
            img = plt.imread(fname)
            return img

def reconstructPointsCloud(rgb_image, depth_image, focal, scale=1000):
    def initColor():
        r = rgb_image[:, :, 0].flatten()
        g = rgb_image[:, :, 1].flatten()
        b = rgb_image[:, :, 2].flatten()
        return np.stack([r, g, b], axis=1)
    def initPoints():
        shape = depth_image.shape
        rows = shape[0]
        cols = shape[1]
        cx, cy = cols / 2, rows / 2
        points = np.zeros((rows*cols, 3), np.float32)

        i = 0
        for r in range(0, rows):
            for c in range(0, cols):
                depth = depth_image[r, c]
                z = (1-depth) * scale
                points[i, 0] = cx + (c - cx) * (z / focal)
                points[i, 1] = cy + (r - cy) * (z / focal)
                points[i, 2] = z
                i = i + 1
        return points
    color = initColor()
    points = initPoints()
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(points)
    pcd.colors = o3d.utility.Vector3dVector(color)
    pcd.transform([
        [1, 0, 0, 0],
        [0,-1, 0, 0],
        [0, 0,-1, 0],
        [0, 0, 0, 1]
        ])
    return pcd

def filterGrandStandPointsCloud(mainPCD, mask):
    return mainPCD.select_by_index( list(np.where(mask.flatten())[0]) )

def fitCrowdPlaneEquation(pcd, th):
    plane_model, inliers = pcd.segment_plane(distance_threshold=th, ransac_n=3, num_iterations=1000)
    [a, b, c, d] = plane_model
    inlier_clouds = pcd.select_by_index(inliers)
    outlier_clouds = pcd.select_by_index(inliers, invert=True)
    return [a, -b, -c, d]


def samplePlanePointsCloud(xBegin, zBegin, xEnd, zEnd, equation, color):
    a, b, c, d = equation
    points_cloud = np.zeros((100*100, 3), np.float32)
    i = 0
    for x in np.linspace(xBegin, xEnd, 100):
        for z in np.linspace(zBegin, zEnd, 100):
            y = -((d + a*x + c*z) / b)
            points_cloud[i, 0] = x
            points_cloud[i, 1] = y
            points_cloud[i, 2] = z
            i = i + 1
    pcd = o3d.geometry.PointCloud()
    pcd.points = o3d.utility.Vector3dVector(points_cloud)
    pcd.paint_uniform_color(color)
    pcd.transform([
        [1, 0, 0, 0],
        [0,-1, 0, 0],
        [0, 0,-1, 0],
        [0, 0, 0, 1]
        ])
    return pcd

def Billboard(depthImg,fg_segment):
    mask = (fg_segment>0)
    depthImg[mask] = 0.0
    depthImg += fg_segment
    return depthImg