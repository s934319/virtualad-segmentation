import subprocess
import argparse
#from field_testing import field_seg
import numpy as np
import cv2,os
from tools import *
from ransac_tools import *
import json

def MakeDir():
    #first layer folder
    os.makedirs('./temp', exist_ok=True)
    os.makedirs(frame_dir, exist_ok=True)
    os.makedirs(annotation_dir, exist_ok=True)
    os.makedirs(firstframe_dir, exist_ok=True)
    os.makedirs(stcnmask_dir, exist_ok=True)
    os.makedirs(depth_dir, exist_ok=True)
    #second layer folder
    os.makedirs(frame2_dir, exist_ok=True)
    os.makedirs(annotation2_dir, exist_ok=True)
    os.makedirs(result_dir,exist_ok=True)

parser = argparse.ArgumentParser()
parser.add_argument('--input', type = str, required=True, help='input video path')
args = parser.parse_args()

if __name__ == '__main__':
    video_dir = args.input
    frame_dir = './temp/frames'
    annotation_dir = './temp/annotation'
    firstframe_dir = './temp/first_frame'
    stcnmask_dir = './temp/stcn_mask'
    depth_dir = './temp/depthmap'
    video_name = video_dir.split('/')[-1].split('.')[0]
    result_dir =  os.path.join('./result',video_name)
    frame2_dir = os.path.join(frame_dir,video_name)
    annotation2_dir = os.path.join(annotation_dir,video_name)
    #part1 : segmentation and STCN
    MakeDir()
    image_list = Video2Frames(video_dir,frame2_dir)
    #prediction segmentation
    firstframe_dir = os.path.join(frame2_dir,'00000.jpg')
    firstframe = cv2.imread(firstframe_dir)
    #copy paste first frame for depth
    cv2.imwrite('./temp/first_frame/00000.jpg',firstframe)
    #first frame annotation
    p0 = subprocess.run(['python','field_testing.py',
                            "--input", './temp/first_frame',
                            "--output", annotation2_dir], shell=False, cwd=os.getcwd())
    print("STCN")
    #STCN subprocess
    p1 = subprocess.run(['python','my_eval.py',
                            "--image_dir", '../temp/frames',
                            "--annotation_dir", '../temp/annotation',
                            "--output", '../temp/stcn_mask'], shell=False, cwd=os.getcwd() + '/STCN')
    #STCN to Depth map
    stcnmask_dir = os.path.join(stcnmask_dir,video_name)
    anno_mask = cv2.imread(os.path.join(annotation2_dir,'00000.png'),0)
    index = pitcher_index(anno_mask)
    cv2.imwrite(os.path.join(stcnmask_dir,'00000.png'),anno_mask) #取代first frame
    filenames = sorted(os.listdir(stcnmask_dir))
    depthmask_list = []
    for filename in filenames:
        mask = cv2.imread(os.path.join(stcnmask_dir,filename),0)
        depthmask_list.append(Gray2Depth(mask,index))
    #make video
    Frames2Video(depthmask_list,result_dir,video_name + '_mask',30)
    Frames2Video(image_list,result_dir,video_name,30)

    #part2 : Depth and RANSAC
    p2 = subprocess.run(['python','run.py',
                        "--Final",
                        "--data_dir", '../temp/first_frame',
                        "--output_dir", '../temp/depthmap'
                        ], shell=False, cwd=os.getcwd() + '/BoostingMonocularDepth')
    rgbImg = np.copy(firstframe)
    depthImg = read_file(os.path.join(depth_dir,'00000.png'))
    semantic_mask = np.load(os.path.join('./temp','semantic.npy'))

    focal = 1000.0
    mainPCD = reconstructPointsCloud(rgbImg, depthImg, focal)

    ground_mask = semantic_mask == 0
    groundPCD = filterGrandStandPointsCloud(mainPCD, ground_mask)
    fieldPlaneEquation = fitCrowdPlaneEquation(groundPCD, 1920/10)

    signboard_mask = semantic_mask == 1
    signboardPCD = filterGrandStandPointsCloud(mainPCD, signboard_mask)
    signboardPlaneEquation = fitCrowdPlaneEquation(signboardPCD, 1920/10)

    auditorium_mask = semantic_mask == 2
    auditoriumPCD = filterGrandStandPointsCloud(mainPCD, auditorium_mask)
    auditoriumPlaneEquation = fitCrowdPlaneEquation(auditoriumPCD, 1920/10)

    datadict = {'field_mask':bounding_box(semantic_mask == 0),\
                 'signboard_mask':bounding_box(semantic_mask == 1),\
                 'auditorium_mask':bounding_box(semantic_mask == 2),
                 'field_normal':fieldPlaneEquation,
                 'signboard_normal':signboardPlaneEquation,
                 'auditorium_normal':auditoriumPlaneEquation,
                 'resolution': rgbImg.shape[0:2]
                }
    
    with open(os.path.join(result_dir,'data.json'), 'w') as f:
        json.dump(datadict, f)

    DeleteFolder('./temp')



