import torch
# import some common detectron2 utilities
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog, DatasetCatalog

import argparse
import cv2,os
import numpy as np
from tools import *

class field_seg:
    def __init__(self):
        MetadataCatalog.get('field_train').set(thing_classes=['field','signboard','auditorium','people'])
        self.field_metadata = MetadataCatalog.get("field_train")
        #Testing cfg
        self.cfg = get_cfg()
        self.cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"))
        self.cfg.MODEL.WEIGHTS = os.path.join("./weight", "instance_test.pth")  # path to the model we just trained
        self.cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.7   # set a custom testing threshold
        self.cfg.MODEL.ROI_HEADS.NUM_CLASSES = 4  # four classes
        self.predictor = DefaultPredictor(self.cfg)
    def predict(self,image):
        outputs = self.predictor(image)
        #Get Masks
        field = np.where((outputs["instances"].pred_classes == 0).cpu().numpy()) ## 0: field
        signboard = np.where((outputs["instances"].pred_classes == 1).cpu().numpy()) ## 1: signboard
        auditorium = np.where((outputs["instances"].pred_classes == 2).cpu().numpy()) ## 2: auditorium
        people = np.where((outputs["instances"].pred_classes == 3).cpu().numpy()) ## 3: person

        self.field_masks = outputs["instances"].pred_masks[field].cpu().detach().numpy()
        self.signboard_masks = outputs["instances"].pred_masks[signboard].cpu().detach().numpy()
        self.auditorium_masks = outputs["instances"].pred_masks[auditorium].cpu().detach().numpy()
        self.people_masks = outputs["instances"].pred_masks[people].cpu().detach().numpy()

        self.result_image = np.full(image.shape[0:2],255)
        self.player_filter()
        self.setmask()
        return self.result_image , np.array(self.player_mask)
    def setmask(self):
        for mask in self.field_masks:
            self.result_image[np.where(mask)] = 0
        for mask in self.signboard_masks:
            self.result_image[np.where(mask)] = 1
        for mask in self.auditorium_masks:
            self.result_image[np.where(mask)] = 2
        for mask in self.people_masks:
            self.result_image[np.where(mask)] = 3
    def player_filter(self):
        def IOU(mask,auditorium_mask):
            intersaction = np.count_nonzero(np.logical_and(mask,auditorium_mask))
            union = np.count_nonzero(mask)
            return intersaction / union
        def Auditorium_mask():
            auditorium_mask = np.full(self.result_image.shape,False)
            for mask in self.auditorium_masks:
                auditorium_mask[np.where(mask)] = True
            return auditorium_mask
        
        self.player_mask = []
        auditorium_mask = Auditorium_mask()
        for mask in self.people_masks:
            if(IOU(mask,auditorium_mask) < 0.5):
                self.player_mask.append(mask)

parser = argparse.ArgumentParser()
parser.add_argument('--input', type = str, required=True, help='input image path')
parser.add_argument('--output', type = str, required=True, help='input image path')
args = parser.parse_args()   
if __name__ == '__main__':
    predictor = field_seg()
    firstframe = cv2.imread(args.input + '/00000.jpg')
    semantic_mask,people_masks = predictor.predict(firstframe)
    anno_mask = graymask(people_masks)
    cv2.imwrite(os.path.join(args.output,'00000.png'),anno_mask)
    np.save(os.path.join('./temp','semantic.npy'),semantic_mask)
        