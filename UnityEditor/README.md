# VirtualAd Unity Editor

### 使用方法
1. 先在foreground segmentation資料夾照指示得到影片計算結果
2. 將計算結果放入"resources"資料夾
3. 找到"Json Reader" GameObject和"Read Json"script
4. 輸入影片名稱並按下"Load Video" 按鈕
5. 啟動模擬器

### 編輯方法
# 解析度設定
請將解析度固定為 1920*1080
# 廣告擺放
將下方圖片拖移入場景中即可生成平面廣告
# 快捷鍵
W: translation tool , E: rotation tool R: scaling tool , Delete: 刪除廣告
# 按鍵
如圖片所示

