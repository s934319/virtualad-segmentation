﻿Shader "Unlit/blending_ez"
{
    Properties
    {
        _AnimTex("Anim", 2D) = "white" {}
        _VideoTex("Video", 2D) = "white" {}
    }
        SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _AnimTex;
            sampler2D _VideoTex;

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                // sample the texture
                fixed4 anim = tex2D(_AnimTex, i.uv);
                fixed4 video = tex2D(_VideoTex, i.uv);

                fixed4 anim_mask = fixed4(0, 0, 0, 0);
                fixed4 black = fixed4(0, 0, 0, 1);
                fixed4 color;
                int color_case = 0;

                //unity-chan
                if (anim.a == 1.0) {
                    anim_mask = fixed4(1, 0, 0, 1);
                    color_case = 1;
                }
                //plane
                if (abs(anim.a - 0.4) < 0.05 && abs(anim.r - 1.0) < 0.1) {
                    color_case = 0;
                    anim_mask = fixed4(0, 0, 0, 1);
                }
                //shadow
                if (abs(anim.a - 0.4) < 0.05 && abs(anim.r) <= 0.05) {
                    color_case = 2;
                    anim_mask = fixed4(0, 1, 0, 1);
                }

                if (color_case == 0)
                    return video;
                if (color_case == 1)
                    return anim;
                if (color_case == 2)
                    return video * 0.5 + black * 0.5;
                //return anim_mask;

                //error
                return fixed4(1.0, 0.2, 0.3, 1);

            }
            ENDCG
        }
    }
}
