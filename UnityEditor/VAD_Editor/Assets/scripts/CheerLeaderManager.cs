﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CheerLeaderManager : MonoBehaviour
{
    // Start is called before the first frame update
    public List<GameObject> cheerLeaders;
    private NavMeshAgent[]  navMeshAgents = new NavMeshAgent [5];
    private Vector3 [] pos = new Vector3[5];
    private Animator[] animators = new Animator[5];
    private int counter = 0;
    private const float danceTime = 10.0f;
    private float remainTime = 0.0f;
    private Vector3 pos_right = new Vector3(-20f, 0f, 36f);
    enum States{
        GotoStage,
        StepForward,
        Dance,
        LeaveStage
    }
    States state;
    void Start()
    {
        for(int i = 0;i < 5;i++){
            navMeshAgents[i] = cheerLeaders[i].GetComponent<NavMeshAgent>();
            animators[i] = cheerLeaders[i].GetComponent<Animator>();
            pos[i] = pos_right + 10f * i * Vector3.right;
        }

        GotoStage();
        state = States.GotoStage;
    }

    void GotoStage()
    {

        foreach (NavMeshAgent navMeshAgent in navMeshAgents)
        {
            navMeshAgent.SetDestination(pos[counter]);
            counter = (counter + 1) % 5;
        }
        return;
    }
    void StepForward()
    {
        foreach (NavMeshAgent navMeshAgent in navMeshAgents)
        {
            navMeshAgent.SetDestination(pos[counter] + 2*Vector3.forward);
            counter = (counter + 1) % 5;
        }
        return;
    }
    void Dance()
    {
        foreach (NavMeshAgent navMeshAgent in navMeshAgents)
        {
            navMeshAgent.isStopped = true;
        }
        foreach (Animator animator in animators)
        {
            animator.SetBool("Dance", true);
        }
        return;
    }
    void LeaveStage()
    {
        foreach (Animator animator in animators)
        {
            animator.SetBool("Dance", false);
        }
        foreach (NavMeshAgent navMeshAgent in navMeshAgents)
        {
            navMeshAgent.isStopped = false;
            navMeshAgent.SetDestination(pos[counter] + 100 * Vector3.right);
            counter = (counter + 1) % 5;
        }
        return;
    }
    private void Update()
    {
        switch (state)
        {
            case States.GotoStage:
                GotoStage();
                if (navMeshAgents[0].remainingDistance < 1.0f)
                    state = States.StepForward;
                break;
            case States.StepForward:
                StepForward();
                if (navMeshAgents[0].remainingDistance < 1.0f)
                    state = States.Dance;
                break;
            case States.Dance:
                Dance();
                if (Timer())
                    state = States.LeaveStage;
                break;
            case States.LeaveStage:
                LeaveStage();
                break;

        }
    }
    bool Timer()
    {
        if (remainTime > danceTime)
            return true;
        else
        {
            remainTime += Time.deltaTime;
            return false;
        }
    }
}
