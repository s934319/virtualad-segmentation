﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Info3D : ScriptableObject
{
    // Start is called before the first frame update
    public Vector3[] field_position = new Vector3[4];
    public Vector3[] signboard_position = new Vector3[4];
    public Vector3[] auditorium_position = new Vector3[4];
}
