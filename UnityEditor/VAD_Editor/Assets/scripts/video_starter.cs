﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class video_starter : MonoBehaviour
{
    // Start is called before the first frame update
    public start_manager videoManager;
    bool loopToggle = true;
    void Start()
    {
        videoManager.Starter += PlayVideo;
        videoManager.Pause += PauseVideo;
        videoManager.Loop += LoopVideo;
    }
    void PlayVideo()
    {
        GetComponent<VideoPlayer>().Play();
    }
    void PauseVideo()
    {
        GetComponent<VideoPlayer>().Pause();
    }
    void LoopVideo()
    {
        GetComponent<VideoPlayer>().isLooping = loopToggle;
        loopToggle = !loopToggle;
    }

}
