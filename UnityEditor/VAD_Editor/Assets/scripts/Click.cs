﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Click : MonoBehaviour
{
    // Start is called before the first frame update
    GameObject Destination;
    public Camera uCamera;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit Hit;
        if (Input.GetMouseButtonDown(1))
        {
            Vector3 pos = Input.mousePosition;
            Ray mRay = uCamera.ScreenPointToRay(pos);
            if (Physics.Raycast(mRay, out Hit))
            {
                transform.position = Hit.point;
                //print(pos);
            }
        }
    }
}
