﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class Drawplane : PrimitiveBase
{
    Info3D baseball1;
    public enum ADTypes
    {
        FIELD,
        SIGNBOARD,
        AUDITORIUM
    }
    public ADTypes types;
    Vector3[] position = new Vector3[4];
    protected override void InitMesh()
    {

        if (meshFilter == null)
        {
            Init();
            InitCorners();
        }

        triangles = new int[] { 0, 1, 2, 2, 1, 3, 0, 2, 1, 1, 2, 3 };

        uvs = new Vector2[]
        {
            new Vector2(0,0),
            new Vector2(1,0),
            new Vector2(1,1),
            new Vector2(0,1)
         };

        CalculateVertices();
    }
    protected override void CalculateVertices()
    {

        vertices = new Vector3[]
        {
            position[0],
            position[1],
            position[2],
            position[3]
        };
        UpdateMesh();
    }
    protected void InitCorners()
    {
        baseball1 = (Info3D)AssetDatabase.LoadAssetAtPath("Assets/3Ddata/baseball1.asset", typeof(Info3D));
        switch (types)
        {
            case (ADTypes.FIELD):
            {
                for(int i = 0; i < 4; i++)
                    position[i] = baseball1.field_position[i];
                break;
            }
            case (ADTypes.SIGNBOARD):
            {
                for (int i = 0; i < 4; i++)
                    position[i] = baseball1.signboard_position[i];
                break;
            }
            case (ADTypes.AUDITORIUM):
            {
                for (int i = 0; i < 4; i++)
                    position[i] = baseball1.auditorium_position[i];
                break;
            }

        }
    }

    private void Update()
    {
        InitCorners();
        CalculateVertices();
        GetComponent<MeshCollider>().sharedMesh = GetComponent<MeshFilter>().sharedMesh;
    }
}
