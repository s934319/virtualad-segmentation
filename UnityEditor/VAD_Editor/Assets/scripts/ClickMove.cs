﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickMove : MonoBehaviour
{
    // Start is called before the first frame update
    NavMeshAgent navMeshAgent;
    Transform Destination;
    Animator animator;
    bool GetPoint = false;
    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        Destination = GameObject.Find("Destination").transform;
    }

    // Update is called once per frame
    void Update()
    {

        navMeshAgent.SetDestination(Destination.position);

        if (navMeshAgent.velocity.magnitude > 0.4f)
            animator.SetBool("Run", true);
        else
            animator.SetBool("Run", false);
    }
}
