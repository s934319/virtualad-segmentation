﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ChanRunning : MonoBehaviour
{
    // Start is called before the first frame update
    Animator anim;
    NavMeshAgent nav;
    start_manager starter;

    private IEnumerator coroutine;
    int point_counter = 0;
    Vector3[] pos = new Vector3[7];
    void Start()
    {
        anim = GetComponent<Animator>();
        nav = GetComponent<NavMeshAgent>();
        starter = GameObject.Find("starter").GetComponent<start_manager>();
        starter.Starter += StartRunning;
        pos[0] = new Vector3( 0, 0, 0);
        pos[1] = new Vector3( 62f, 0, -1.5f);
        pos[2] = new Vector3( 62f, 0,  61.1f);
        pos[3] = new Vector3(-62f, 0,  61.1f); 
        pos[4] = new Vector3(-27.8f, 0,  106.6f);
        pos[5] = new Vector3(39.3f, 0, 106.6f);
        pos[6] = new Vector3(29.0f, 0, 116.4f);
    }
    void StartRunning()
    {
        StartCoroutine(running());
    }
    private IEnumerator running()
    {
        anim.SetBool("run", true);
        nav.SetDestination(pos[point_counter]);
        while (true)
        {
            if (nav.remainingDistance < 1.0f)
            {
                point_counter++;
                if (point_counter < pos.Length)
                nav.SetDestination(pos[point_counter]);
                else
                    win();
            }
            yield return null;
        }

    }

    private IEnumerator winning()
    {
        anim.SetTrigger("win");
        yield return null;
    }
    private void win()
    {
        StopAllCoroutines();
        coroutine = winning();
        StartCoroutine(coroutine);
    }
}
