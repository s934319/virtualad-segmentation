﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class QuadMesh2 : PrimitiveBase
{
    public Vector3 normal;
    public int index;
    Vector3 leftcorner;
    Vector3 rightcorner;
    Info3D baseball1;

    public float Width;
    public float Length;
    public bool update;
    protected override void InitMesh()
    {
        
        if (meshFilter == null)
        {
            Init();
            InitCorners();
        }

        triangles = new int[] { 0, 1, 2 , 2, 1, 3, 0, 2, 1, 1, 2, 3};

        uvs = new Vector2[]
        {
            new Vector2(0,0),
            new Vector2(1,0),
            new Vector2(1,1),
            new Vector2(0,1)
         };

        CalculateVertices();
    }
    protected override void CalculateVertices()
    {
        var x = Width * 1f;
        var z = Length * 1f;
        Vector3 reference = rightcorner - leftcorner;
        Vector3 up = Vector3.Cross(reference, normal).normalized;
        Vector3 right = Vector3.Cross(normal, up).normalized;
        
        vertices = new Vector3[]
        {
            leftcorner,
            leftcorner + up*x,
            leftcorner + right*z,
            leftcorner  + right*z + up*x
        };
        UpdateMesh();
    }
    protected void InitCorners()
    {
        baseball1 = (Info3D)AssetDatabase.LoadAssetAtPath("Assets/3Ddata/baseball1.asset", typeof(Info3D));

    }

    private void Update()
    {
        if (update)
        {
            InitCorners();
            if (index == 0)
            {
                leftcorner = baseball1.field_position[1];
                rightcorner = baseball1.field_position[3];
            }
            else
            {
                leftcorner = baseball1.signboard_position[1];
                rightcorner = baseball1.signboard_position[3];
            }
            CalculateVertices();
        }

    }

}
