﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ad_manager : MonoBehaviour
{
    public void ClearAds()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Destroy(transform.GetChild(i).gameObject);
        }
    }
}
