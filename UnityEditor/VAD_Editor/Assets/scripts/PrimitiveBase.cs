﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
[ExecuteInEditMode]
public class PrimitiveBase : MonoBehaviour
{
    protected MeshFilter meshFilter;

    protected Vector3[] vertices;
    protected int[] triangles;
    protected Vector2[] uvs;
    private void Awake()
    {
        Init();
        InitMesh();
    }
    protected void Init()
    {
        meshFilter = GetComponent<MeshFilter>();
        meshFilter.sharedMesh = new Mesh();
    }
    protected virtual void InitMesh() { }
    protected virtual void CalculateVertices() { }
    protected void UpdateMesh()
    {
        if (meshFilter.sharedMesh == null)
        {
            meshFilter.sharedMesh = new Mesh();
        }
        meshFilter.sharedMesh.vertices = vertices;
        meshFilter.sharedMesh.triangles = triangles;
        meshFilter.sharedMesh.uv = uvs;
    }
    private void Update()
    {
        InitMesh();
    }
}
