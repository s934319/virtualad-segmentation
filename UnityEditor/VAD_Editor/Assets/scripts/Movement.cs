﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Movement : MonoBehaviour
{
    // Start is called before the first frame update
    Vector3[] pos = new Vector3[6];
    int state = 0;
    NavMeshAgent navMeshAgent;
    //Animator anim;
    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        
        pos[0] = new Vector3(-84.23529f, 0.08333334f, -84.36079f);
        pos[1] = new Vector3(-8.335392f, 0.08333334f, -105.3301f);
        pos[2] = new Vector3(61.9971f, 0.08333334f,   -84.72742f);
        pos[3] = new Vector3(-14.8242f, 0.08333334f, -57.34396f);
        pos[4] = new Vector3(-60.1f, 0.08333334f, -11.6f);
        navMeshAgent.SetDestination(pos[0]);

    }

    // Update is called once per frame
    void Update()
    {
        if (navMeshAgent.remainingDistance < 0.5f)
        {
            if(state <= 4)
            {
                state += 1;
                navMeshAgent.SetDestination(pos[state]);
            }
                
        }
            
    }
}
