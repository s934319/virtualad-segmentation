﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Drawline : MonoBehaviour
{
    // Start is called before the first frame update

    Vector3 field_pos1;
    Vector3 field_pos2;
    Vector3 field_pos3;
    Vector3 field_pos4;

    Vector3 signboard_pos2;
    Vector3 signboard_pos4;

    Vector3 auditorium_pos2;
    Vector3 auditorium_pos4;

    string segment;

    public Camera uCamera;
    Info3D baseball_3D;
    public SegData baseball_seg;

    QuadMesh field;
    QuadMesh signboard;
    QuadMesh auditorium;

    bool signboard_flag = true;
    bool auditorium_flag = true;

    public void SetData(SegData segData)
    {
        baseball_seg = segData;
    }
    void OnDrawGizmos()
    {
        Init();
        DrawField();
        DrawSignboard();
        DrawAuditorium();
    }
    private void Init()
    {
        baseball_3D = (Info3D)AssetDatabase.LoadAssetAtPath("Assets/3Ddata/baseball1.asset", typeof(Info3D));
        segment = "ad_scene/segmentation/";
        field = GameObject.Find(segment + "field").GetComponent<QuadMesh>();
        signboard = GameObject.Find(segment + "signboard").GetComponent<QuadMesh>();
        auditorium = GameObject.Find(segment + "auditorium").GetComponent<QuadMesh>();
    }
    void DrawField()
    {
        //Drawing
        field.normal = baseball_seg.normals[0];
        field.UpdateMeshCollider();
        //init pos and Ray
        float raw_y = baseball_seg.signboard_position[0].y;
        float y = YTransform(baseball_seg.height, raw_y);
        Vector3 pos = new Vector3(0, 0 ,0);
        Vector3 pos2 = new Vector3(0, y, 0);
        Vector3 pos3 = new Vector3(1920, 0, 0);
        Vector3 pos4 = new Vector3(1920, y, 0);

        Ray mRay = uCamera.ScreenPointToRay(pos);
        Ray mRay2 = uCamera.ScreenPointToRay(pos2);
        Ray mRay3 = uCamera.ScreenPointToRay(pos3);
        Ray mRay4 = uCamera.ScreenPointToRay(pos4);

        RaycastHit Hit;
        int layerMask = 1 << 9;
        //Get 4 positions in 3D space
        signboard_flag = true;
        if (Physics.Raycast(mRay, out Hit, Mathf.Infinity, layerMask))
        {
            field_pos1 = Hit.point;
        }
        else
            NotFound(true);
        if (Physics.Raycast(mRay2, out Hit, Mathf.Infinity, layerMask))
        {
            field_pos2 = Hit.point;
        }
        else
            NotFound(true);
        if (Physics.Raycast(mRay3, out Hit, Mathf.Infinity, layerMask))
        {
            field_pos3 = Hit.point;
        }
        else
            NotFound(true);
        if (Physics.Raycast(mRay4, out Hit, Mathf.Infinity, layerMask))
        {
            field_pos4 = Hit.point;
        }
        else
            NotFound(true);
        // Draw Gizmos
        Gizmos.color = Color.red;
        Gizmos.DrawLine(field_pos1, field_pos2);
        Gizmos.DrawLine(field_pos1, field_pos3);
        Gizmos.DrawLine(field_pos3, field_pos4);
        Gizmos.DrawLine(field_pos4, field_pos2);
    }
    void DrawSignboard()
    {
        if (signboard_flag)
        {
            signboard.enabled = true;
            signboard.normal = baseball_seg.normals[1];
            signboard.UpdateMeshCollider();
            Vector3 temp = (signboard.MidPoint((field_pos2 + field_pos4) / 2));
            signboard.SetPosition(temp);

            float raw_y = baseball_seg.signboard_position[3].y;
            float y2 = YTransform(baseball_seg.height, raw_y);

            Vector3 pos2 = new Vector3(0, y2, 0);
            Vector3 pos4 = new Vector3(1920, y2, 0);

            Ray mRay2 = uCamera.ScreenPointToRay(pos2);
            Ray mRay4 = uCamera.ScreenPointToRay(pos4);

            RaycastHit Hit;
            int layerMask = 1 << 10;
            auditorium_flag = true;
            if (Physics.Raycast(mRay2, out Hit, Mathf.Infinity, layerMask))
            {
                signboard_pos2 = Hit.point;
            }
            else
            {
                NotFound(false);
            }

            if (Physics.Raycast(mRay4, out Hit, Mathf.Infinity, layerMask))
            {
                signboard_pos4 = Hit.point;
            }
            else
            {
                NotFound(false);
            }
            Gizmos.color = Color.black;
            Gizmos.DrawLine(signboard_pos2, field_pos2);
            Gizmos.DrawLine(signboard_pos4, field_pos4);
            Gizmos.DrawLine(signboard_pos2, signboard_pos4);

            baseball_3D.signboard_position[0] = field_pos2;
            baseball_3D.signboard_position[1] = field_pos4;
            baseball_3D.signboard_position[2] = signboard_pos2;
            baseball_3D.signboard_position[3] = signboard_pos4;
        }
        else
        {
            signboard.enabled = false;
        }
    }
    void DrawAuditorium()
    {
        if (auditorium_flag)
        {
            auditorium.enabled = true;
            auditorium.normal = baseball_seg.normals[2];
            auditorium.UpdateMeshCollider();
            Vector3 temp = (auditorium.MidPoint((signboard_pos2 + signboard_pos4) / 2));
            auditorium.SetPosition(temp);

            Vector3 pos = new Vector3(0, 1080, 0);
            Vector3 pos3 = new Vector3(1920, 1080, 0);
            Ray mRay = uCamera.ScreenPointToRay(pos);
            Ray mRay3 = uCamera.ScreenPointToRay(pos3);
            int layerMask = 1 << 11;
            RaycastHit Hit;
            if (Physics.Raycast(mRay, out Hit, Mathf.Infinity, layerMask))
            {
                auditorium_pos2 = Hit.point;
            }
            if (Physics.Raycast(mRay3, out Hit, Mathf.Infinity, layerMask))
            {
                auditorium_pos4 = Hit.point;
            }
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(signboard_pos2, auditorium_pos2);
            Gizmos.DrawLine(signboard_pos4, auditorium_pos4);
            Gizmos.DrawLine(auditorium_pos2, auditorium_pos4);
        }
    }
    float YTransform(int resolution , float pixel_y)
    {
        return 1080f - (1080f / resolution) * pixel_y;
    }
    void NotFound(bool signboard)
    {
        if (signboard)
        {
            signboard_flag = false;
            Debug.LogError("Can't find hit point");
        }
        else
        {
            auditorium_flag = false;
            Debug.LogError("Can't find hit point");
        }
    }
}

