﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickSeg : MonoBehaviour
{
    public Camera uCamera;
    public GameObject prefab;
    GameObject bindObj;
    GameObject ADs;
    Vector3 bindMousePos;
    Vector3 bindPos;
    int bindlayer;

    Vector3 normal;
    Vector3 up;
    Vector3 right;

    public Vector3[] normals = new Vector3 [3];

    public Material ad_material;
    public Material ground_material;
    // Update is called once per frame
    private void Start()
    {
        ADs = GameObject.Find("ad_scene/ADs");
    }
    void Update()
    {
        //MouseDownCallback();
        //MouseMoveCallback();
        //KeyCallBack();
        //MouseUpCallBack();

    }
    public void SetNormal(Vector3[] json_normals)
    {
        for (int i = 0;i<3;i++)
            normals[i] = json_normals[i];
    }
    void MouseDownCallback()
    {
        int layerMask = (1 << 10) + 1;
        int layerMask2 = 1;
        layerMask = ~layerMask;

        RaycastHit Hit;
        Vector3 pos = Input.mousePosition;
        Ray mRay = uCamera.ScreenPointToRay(pos);

        if (Input.GetMouseButtonDown(0))
        {
            if (Physics.Raycast(mRay, out Hit, Mathf.Infinity, layerMask2))
            {
                //bind AD
                print(Hit.collider.name);
                if (Hit.collider.gameObject.tag == "AD")
                {
                    bindObj = Hit.collider.gameObject;
                    bindMousePos = Input.mousePosition;
                    bindPos = Hit.collider.transform.position;
                    
                }
            }
            if (Physics.Raycast(mRay, out Hit, Mathf.Infinity, layerMask))
            {
                //bind AD
                bindlayer = Hit.collider.gameObject.layer;
                NormalAssign(bindlayer);
            }
        }
        
    }
    void MouseMoveCallback()
    {
        int layerMask = (1 << 10) + 1;
        layerMask = ~layerMask;

        RaycastHit Hit;
        Vector3 pos = Input.mousePosition;
        Ray mRay = uCamera.ScreenPointToRay(pos);
        if (Input.GetMouseButton(0))
        {
            
            if (Physics.Raycast(mRay, out Hit, Mathf.Infinity, layerMask))
            {
                //bind AD
                int layercheck;
                layercheck = Hit.collider.gameObject.layer;

                if (layercheck == bindlayer && bindObj != null)
                {
                    Vector3 movement = Input.mousePosition - bindMousePos;
                    //float delta = 0.1f;
                    bindObj.transform.position = Hit.point + normal*0.01f;
                }
                else if(layercheck != bindlayer && bindObj != null)
                {
                    bool fieldcheck = FieldCheck(layercheck);
                    Texture texture = bindObj.GetComponent<MeshRenderer>().material.mainTexture;
                    if (fieldcheck)
                    {
                        bindObj.GetComponent<MeshRenderer>().material = ground_material;
                        bindObj.GetComponent<MeshRenderer>().material.mainTexture = texture;
                    }
                    else
                    {
                        bindObj.GetComponent<MeshRenderer>().material = ad_material;
                        bindObj.GetComponent<MeshRenderer>().material.mainTexture = texture;
                    }
                    NormalAssign(layercheck);
                    RecreateAD(Hit.point);
                    bindlayer = layercheck;
                    ResetCollider();
                }

            }
            
        }
    }
    void MouseUpCallBack()
    {
        if (Input.GetMouseButtonUp(0))
        {
            //unbind AD
            bindObj = null;
            bindPos = Vector3.zero;
        }
    }
    void KeyCallBack()
    {
        if(bindObj != null)
        {
            QuadMesh quadMesh = bindObj.GetComponent<QuadMesh>();
            if (Input.GetKeyDown(KeyCode.W))
            {
                quadMesh.Width += 5;
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                quadMesh.Width -= 5;
            }
            if (Input.GetKeyDown(KeyCode.A))
            {
                quadMesh.Length -= 5;
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                quadMesh.Length += 5;
            }
            if (Input.GetKeyDown(KeyCode.Delete))
            {
                Destroy(bindObj);
                return;
            }
        }

    }
    void NormalAssign(int layer)
    {
        switch (layer)
        {
            //field
            case 9:
            {
                normal = normals[0];
                break;
            }
            case 12:
            {
                normal = normals[1];
                break;
            }
            case 11:
            {
                normal = normals[2];
                break;
            }
        }
        right = Vector3.Cross(Vector3.up, normal).normalized;
        up = Vector3.Cross(normal, right).normalized;
    }
    float AngleAssign(int layer)
    {
        float rotateY = 0.0f;
        switch (layer)
        {
            //field
            case 9:
                {
                    rotateY = Vector3.Angle(Vector3.up, normals[0]);
                    break;
                }
            case 12:
                {
                    rotateY = Vector3.Angle(Vector3.up, normals[1]);
                    break;
                }
            case 11:
                {
                    rotateY = Vector3.Angle(Vector3.up, normals[2]);
                    break;
                }
        }
        return rotateY;
    } 
    void RecreateAD(Vector3 pos)
    {
        QuadMesh quadMesh = bindObj.GetComponent<QuadMesh>();
        quadMesh.normal = normal;
        bindPos = pos;
    }
    private void ResetCollider()
    {
        bindObj.GetComponent<MeshCollider>().sharedMesh = bindObj.GetComponent<MeshFilter>().sharedMesh;
    }

    public void CreateAD(Vector3 screen_pos, Texture texture)
    {
        int layerMask = (1 << 10) + 1;
        layerMask = ~layerMask;

        RaycastHit Hit;
        Vector3 pos = screen_pos;
        Ray mRay = uCamera.ScreenPointToRay(pos);


        if (Physics.Raycast(mRay, out Hit, Mathf.Infinity, layerMask))
        {
            //bind AD
            bindlayer = Hit.collider.gameObject.layer;
            NormalAssign(bindlayer);
        }
        
        GameObject Ad = Instantiate(prefab);
        Ad.transform.SetParent(ADs.transform);

        float rotateX = AngleAssign(bindlayer);
        Ad.transform.eulerAngles = new Vector3(rotateX, 180f, 0f);
        bool fieldcheck =  FieldCheck(bindlayer);
        MeshRenderer meshRenderer = Ad.GetComponent<MeshRenderer>();
        if (fieldcheck)
        {
            meshRenderer.material = ground_material;
            meshRenderer.material.mainTexture = texture;
            Ad.transform.position = Hit.point + normal * 0.1f;
        }
        else
        {
            meshRenderer.material = ad_material;
            meshRenderer.material.mainTexture = texture;
            Ad.transform.position = Hit.point + normal * 0.1f;
        }
    }
    bool FieldCheck(int layer)
    {
        if (layer == 9)
            return true;
        else
            return false;
    }
}
