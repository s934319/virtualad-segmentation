﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ReadJson))]
public class BuilderEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        ReadJson readJson = (ReadJson)target;
        if(GUILayout.Button("Load Video"))
        {
            readJson.Load(readJson.filename);
        }
    }
}
