﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SegData : ScriptableObject
{
    public Vector3[] normals = new Vector3[3];
    public Vector2[] signboard_position = new Vector2 [4];
    public int height;
}
