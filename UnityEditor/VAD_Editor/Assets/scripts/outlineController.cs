﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class outlineController : MonoBehaviour
{
    // Start is called before the first frame update
    public MeshRenderer meshRenderer;
    private void Start()
    {
        OutlineToggle(false);
    }
    public void OutlineToggle(bool toggle)
    {
        if (toggle)
            meshRenderer.enabled = true;
        else
            meshRenderer.enabled = false;
    }
}
