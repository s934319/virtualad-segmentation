﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetUniform : MonoBehaviour
{
    // Start is called before the first frame update
    public CanvasRenderer canvasRenderer;
    public Camera uCamera;
    public float pitcher_screenppos;
    public int batter_screenppos;
    public float pitcher_estDepth;
    public float batter_estDepth;
    
    Vector3 pitcher_pos;
    Vector3 batter_pos;
    void Start()
    {
        RaycastHit Hit;
        Vector3 pos = new Vector3(540,1080 -pitcher_screenppos,0);
        Ray mRay = uCamera.ScreenPointToRay(pos);
        if (Physics.Raycast(mRay, out Hit))
        {
            pitcher_pos = Hit.point;
            print(pitcher_pos.z);
        }
        pos = new Vector3(540,1080 - batter_screenppos, 0);
        mRay = uCamera.ScreenPointToRay(pos);
        if (Physics.Raycast(mRay, out Hit))
        {
            batter_pos = Hit.point;
        }
    }

    // Update is called once per frame
    void Update()
    {
        float depth = Normalize(transform.position.z);
        //print(depth);
        canvasRenderer.GetMaterial(0).SetFloat("depth", depth);
    }
    float Normalize(float z)
    {
        return (pitcher_estDepth - batter_estDepth) / (pitcher_pos.z - batter_pos.z) * (z - batter_pos.z) + batter_estDepth;
    }
}
