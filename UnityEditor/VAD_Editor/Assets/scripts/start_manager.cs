﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class start_manager : MonoBehaviour
{
    // Start is called before the first frame update
    public delegate void start_manage();

    public event start_manage Starter;
    public event start_manage Pause;
    public event start_manage Loop; 
    void Start()
    {
        
    }

    // Update is called once per frame

    public void StartButton()
    {
        Starter?.Invoke();
    }
    public void PauseButton()
    {
        Pause?.Invoke();
    }
    public void LoopButton()
    {
        Loop?.Invoke();
    }
}
