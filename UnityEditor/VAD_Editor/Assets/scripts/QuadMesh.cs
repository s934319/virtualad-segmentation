﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuadMesh : PrimitiveBase
{
    public Vector3 normal;
    Vector3 up;
    Vector3 right;

    public float Length;
    public float Width;
    
    protected override void InitMesh()
    {
        if (meshFilter == null)
        {
            Init();
        }

        triangles = new int[] { 0, 3, 2, 0, 2, 1 };

        uvs = new Vector2[]
        {
            new Vector2(0,0),
            new Vector2(0,1),
            new Vector2(1,1),
            new Vector2(1,0)
         };

        CalculateVertices();
    }
    protected override void CalculateVertices()
    {
        var z = Width * 0.5f;
        var x = Length * 0.5f;
        Vector3 reference = Vector3.forward;
        up = -Vector3.Cross(normal, reference).normalized;
        right = Vector3.Cross(normal, up).normalized;

        vertices = new Vector3[]
        {
            -right*x - up*z,
             right*x - up*z,
             right*x + up*z,
            -right*x + up*z
        };
        UpdateMesh();
    }
    public void SetPosition(Vector3 pos)
    {
        transform.position = pos;
    }
    public void UpdateMeshCollider()
    {
        GetComponent<MeshCollider>().sharedMesh = GetComponent<MeshFilter>().sharedMesh;
    }
    public Vector3 MidPoint(Vector3 buttom_center)
    {
        return buttom_center + right * Width * 0.25f;
    }

}
