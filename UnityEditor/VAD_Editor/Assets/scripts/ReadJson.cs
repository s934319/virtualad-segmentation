﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Video;
using System.IO;
public class ReadJson : MonoBehaviour
{
    // Start is called before the first frame update
    //public ClickSeg clickSeg;
    public string filename;
    private class SaveObject
    {
        public int[] field_mask = new int [4];
        public int[] signboard_mask = new int[4];
        public int[] auditorium_mask = new int[4];
        public float[] field_normal = new float[4];
        public float[] signboard_normal = new float[4];
        public float[] auditorium_normal = new float[4];
        public int[] resolution = new int[2];
    }
     public void Load(string filename)
    {
        ClickSeg clickSeg = GameObject.Find("ad_scene/Click").GetComponent<ClickSeg>();
        SegData baseball_seg = new SegData();
        VideoPlayer baseballPlayer = GameObject.Find("VideoPlayers/Video Player").GetComponent<VideoPlayer>();
        VideoPlayer maskPlayer = GameObject.Find("VideoPlayers/Video Player(Mask)").GetComponent<VideoPlayer>();
        Drawline drawline = GameObject.Find("ad_scene/Indicator").GetComponent<Drawline>();
        string filedir = Application.dataPath + "/resources/" + filename + "/data.json";
        if(File.Exists(filedir)){
            string saveString = File.ReadAllText(filedir);
        
            SaveObject saveObject = JsonUtility.FromJson<SaveObject>(saveString);
            //Load Normal
            baseball_seg.normals[0] = PostProcessing(saveObject.field_normal);
            //baseball_seg.normals[1] = PostProcessing(saveObject.signboard_normal);
            baseball_seg.normals[1] = Vector3.Cross(baseball_seg.normals[0],Vector3.right).normalized;
            baseball_seg.normals[2] = PostProcessing(saveObject.auditorium_normal);

            baseball_seg.signboard_position[0].y = saveObject.signboard_mask[3];
            baseball_seg.signboard_position[1].y = saveObject.signboard_mask[3];
            baseball_seg.signboard_position[2].y = saveObject.signboard_mask[2];
            baseball_seg.signboard_position[3].y = saveObject.signboard_mask[2];
            baseball_seg.height = saveObject.resolution[0];

            //Load Video
            VideoClip baseball_video = Resources.Load<VideoClip>(filename + "/" + filename);
            VideoClip mask_video = Resources.Load<VideoClip>(filename + "/" + filename + "_mask");
            drawline.SetData(baseball_seg);
            clickSeg.SetNormal(baseball_seg.normals);
            baseballPlayer.clip = baseball_video;
            maskPlayer.clip = mask_video;
            print("Loaded");
        }
        else
        {
            Debug.LogError("No such file");
        }
    }
    private Vector3 PostProcessing(float[] normal)
    {
        float y = Mathf.Abs(normal[1]);
        float z = -Mathf.Abs(normal[2]);
        Vector3 new_normal = new Vector3(0, y, z);
        return new_normal.normalized;
    }
}

