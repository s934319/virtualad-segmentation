﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ItemDropHandler : MonoBehaviour, IDropHandler
{
    // Start is called before the first frame update
    ClickSeg clickSeg;
    RawImage rawImage;
    private void Start()
    {
        clickSeg = GameObject.Find("ad_scene/Click").GetComponent<ClickSeg>();
        rawImage = GetComponent<RawImage>();
    }
    public void OnDrop(PointerEventData eventData)
    {
        clickSeg.CreateAD(Input.mousePosition , rawImage.mainTexture);
    }
}
