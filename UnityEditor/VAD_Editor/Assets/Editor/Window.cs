﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class DesignWindow : EditorWindow
{
    Rect headerSection;
    Rect remainingSection;

    Material material;
    static ADdata adData;
    public static ADdata ADinfo { get { return adData; } }

    Texture2D headerTexture;
    static GameObject canvas;

    int index = 0;
    public static string[] options;
    string dataPath = "Assets/resources/";
    Color headerSectionColor = new Color(13f / 255f, 32f / 255f, 44f / 255f, 1f);
    
    [MenuItem("Window/AD Designer")]
    static void OpenWindow()
    {
        DesignWindow window = (DesignWindow)GetWindow(typeof(DesignWindow));
        window.minSize = new Vector2(300, 400);
        window.Show();
    }
    private void OnEnable()
    {
        canvas = GameObject.Find("ADs/Canvas2");
        InitTexture();
        InitData();
    }
    private void OnGUI()
    {
        DrawLayout();
        DrawHeader();
        DrawSettings();
    }
    void InitTexture()
    {
        headerTexture = new Texture2D(1, 1);
        headerTexture.SetPixel(0, 0, headerSectionColor);
        headerTexture.Apply();
    }
    void DrawLayout()
    {
        headerSection.x = 0;
        headerSection.y = 0;
        headerSection.width = Screen.width;
        headerSection.height = 50;
        GUI.DrawTexture(headerSection, headerTexture);
        remainingSection.x = 0;
        remainingSection.y = 50;
        remainingSection.width = Screen.width;
        remainingSection.height = Screen.height - 50;
    }
    void DrawHeader()
    {
        GUILayout.BeginArea(headerSection);
        GUILayout.Label("AD Creater");
        GUILayout.EndArea();
    }
    void DrawSettings()
    {
        GUILayout.BeginArea(remainingSection);

        GUILayout.BeginHorizontal();
        GUILayout.Label("Image");
        adData.texture = (Texture2D) EditorGUILayout.ObjectField(adData.texture, typeof(Texture2D),false);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("AD Type");
        adData.adTypes = (ADdata.ADTypes)EditorGUILayout.EnumPopup(adData.adTypes);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Create" , GUILayout.Height(20)))
        {
            if(adData.texture != null)
            {
                GameObject image = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/prefab/RawImage.prefab", typeof(GameObject));
                //Init Data
                GameObject imagePrefab = (GameObject) PrefabUtility.InstantiatePrefab(image);
                RawImage rawImage = imagePrefab.GetComponent<RawImage>();
                if(adData.adTypes == ADdata.ADTypes.FIELD)
                {
                    material = new Material(Shader.Find("Unlit/GroundAD"));
                }
                else
                {
                    material = new Material(Shader.Find("Unlit/AD"));
                }
                InitValue(adData.adTypes);
                material.mainTexture = adData.texture;
                rawImage.material = material;
                rawImage.texture = adData.texture;
                imagePrefab.transform.SetParent(canvas.transform);
                GeneralSettings.OpenWindow(imagePrefab,false);
            }
        }
        GUILayout.EndHorizontal();

        GUILayout.EndArea();
    }
    public static void InitData()
    {
        adData = (ADdata)CreateInstance(typeof(ADdata));
    }
    public static void InitValue(ADdata.ADTypes type)
    {
        switch(type)
        {
            case ADdata.ADTypes.FIELD:
            {
                adData.rotateX = 85;
                adData.PosX = 100;
                adData.PosY = 100;
                break;
            }
            case ADdata.ADTypes.SIGNBOARD:
            {
                adData.rotateX = 0;
                adData.PosX = 100;
                adData.PosY = 670;
                break;
            }
            case ADdata.ADTypes.AUDITORIUM:
            {
                adData.rotateX = 32.8f;
                adData.PosX = 100;
                adData.PosY = 800;
                break;
            }


        }
    }

    public static List<string> GetAllChildsName(GameObject Go)
    {
        List<string> list = new List<string>();
        for (int i = 0; i < Go.transform.childCount; i++)
        {
            list.Add(Go.transform.GetChild(i).gameObject.name);
        }
        return list;
    }
}

public class GeneralSettings : EditorWindow
{
    static GameObject editImage;
    static ADdata data;
    string dataPath = "Assets/resources/";
    static bool windowmode;
    public static void OpenWindow(GameObject image,bool mode)
    {
        GeneralSettings window = (GeneralSettings)GetWindow(typeof(GeneralSettings));
        data = DesignWindow.ADinfo;
        editImage = image;
        windowmode = mode;
        window.minSize = new Vector2(200, 400);
        InitData();
        window.Show();
    }
    private void OnGUI()
    {
        DrawSettings(DesignWindow.ADinfo);
        if (editImage != null)
        {
            UpdateData();
        }
    }

    void UpdateData()
    {
        RectTransform rect = editImage.GetComponent<RectTransform>();
        rect.anchoredPosition3D = new Vector2(data.PosX, data.PosY);
        rect.sizeDelta = new Vector2(data.width, data.height);
        rect.localEulerAngles = new Vector3(data.rotateX, data.rotateY, data.rotateZ);
    }
    static void InitData()
    {
        RectTransform rect = editImage.GetComponent<RectTransform>();
        rect.position = Vector3.zero;
        rect.localScale = new Vector3(1.0f, 1.0f, 1.0f);
    }
    void DrawSettings(ADdata adData)
    {
        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("PosX");
        adData.PosX = EditorGUILayout.Slider(adData.PosX, 0, 1920);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("PosY");
        adData.PosY = EditorGUILayout.Slider(adData.PosY, 0, 1080);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("rotateX");
        adData.rotateX = EditorGUILayout.Slider(adData.rotateX, 0, 180);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("rotateY");
        adData.rotateY = EditorGUILayout.Slider(adData.rotateY, 0, 180);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("rotateZ");
        adData.rotateZ = EditorGUILayout.Slider(adData.rotateZ, 0, 180);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("width");
        adData.width = EditorGUILayout.FloatField(adData.width);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("height");
        adData.height = EditorGUILayout.FloatField(adData.height);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("ADname");
        adData.ADname = EditorGUILayout.TextField(adData.ADname);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        string savePath = dataPath + adData.ADname + ".asset";
        if (GUILayout.Button("Save", GUILayout.Height(20)))
        {
            //save data
            if (!windowmode)
            {
                if (string.IsNullOrEmpty(AssetDatabase.AssetPathToGUID(savePath)))
                {
                    editImage.name = adData.ADname;
                    AssetDatabase.CreateAsset(adData, savePath);
                    DesignWindow.InitData();
                    this.Close();
                }
                else
                {
                    Debug.LogWarning("File exist!");

                }
            }
            else
            {
                DesignWindow.InitData();
                this.Close();
            }
        }

        if (GUILayout.Button("Cancel", GUILayout.Height(20)))
        {
            //cancel
            if (!windowmode)
            {
                DestroyImmediate(editImage);
                DesignWindow.InitData();
                this.Close();
            }
            else
            {
                DesignWindow.InitData();
                this.Close();
            }
            
        }
        EditorGUILayout.EndHorizontal();
    }
}
