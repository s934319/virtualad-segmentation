﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ADdata : ScriptableObject
{
    public enum ADTypes
    {
        FIELD,
        SIGNBOARD,
        AUDITORIUM
    }
    public ADTypes adTypes;
    public Texture2D texture;

    public float rotateX;
    public float rotateY;
    public float rotateZ;

    public float PosX;
    public float PosY;

    public float width = 300;
    public float height = 300;

    public string ADname;
}
